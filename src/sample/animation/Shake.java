package sample.animation;

import javafx.animation.TranslateTransition;
import javafx.scene.Node;
import javafx.util.Duration;

/**
 * Класс, описывающий анимацию управляющего элемента
 */
public class Shake {
    private TranslateTransition transition;

    public Shake(Node node) {
        transition = new TranslateTransition(Duration.millis(100), node);
        transition.setFromX(0f);
        transition.setFromY(0f);
        transition.setByX(10f);
        transition.setByY(10f);
        transition.setCycleCount(3);
        transition.setAutoReverse(true);
    }

    public void playAnimation() {
        transition.playFromStart();
    }
}

