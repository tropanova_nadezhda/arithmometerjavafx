package sample.calculator;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

public class Controller {

    private String First;
    private String action;
    private double f;
    private double result;

    @FXML
    private Label text;

    @FXML
    void onClickC(MouseEvent event) {
        text.setText("");
    }

    @FXML
    void onClickCE(MouseEvent event) {
        String t = text.getText();

        if (t.isEmpty()) {
            text.setText("0");
        } else {
            text.setText(t.substring(0, t.length() - 1));
        }
    }

    @FXML
    void onClickZero(MouseEvent event) {
        text.setText(text.getText() + "0");
    }

    @FXML
    void onClickOne(MouseEvent event) {
        text.setText(text.getText() + "1");
    }

    @FXML
    void onClickTwo(MouseEvent event) {
        text.setText(text.getText() + "2");
    }

    @FXML
    void onClickThree(MouseEvent event) {
        text.setText(text.getText() + "3");
    }

    @FXML
    void onClickFour(MouseEvent event) {
        text.setText(text.getText() + "4");
    }

    @FXML
    void onClickFive(MouseEvent event) {
        text.setText(text.getText() + "5");
    }

    @FXML
    void onClickSix(MouseEvent event) {
        text.setText(text.getText() + "6");
    }

    @FXML
    void onClickSeven(MouseEvent event) {
        text.setText(text.getText() + "7");
    }

    @FXML
    void onClickEight(MouseEvent event) {
        text.setText(text.getText() + "8");
    }

    @FXML
    void onClickNine(MouseEvent event) {
        text.setText(text.getText() + "9");
    }

    @FXML
    void onClickPoint(MouseEvent event) {
        text.setText(text.getText() + ".");
    }

    @FXML
    void onClickPlus(MouseEvent event) {
        First = text.getText();
        f = Double.parseDouble(First);
        action = "+";
        text.setText("");
    }

    @FXML
    void onClickMinus(MouseEvent event) {
        First = text.getText();
        f = Double.parseDouble(First);
        action = "-";
        text.setText("");
    }
    @FXML
    void onClickPlusMinus(MouseEvent event) {
        text.setText(text.getText() + "-");
    }

    @FXML
    void onClickMultiply(MouseEvent event) {
        First = text.getText();
        f = Double.parseDouble(First);
        action = "*";
        text.setText("");
    }

    @FXML
    void onClickDivision(MouseEvent event) {
        First = text.getText();
        f = Double.parseDouble(First);
        action = "/";
        text.setText("");
    }

    @FXML
    void onClickEqually(MouseEvent event) {
        String last = text.getText();
        double l = Double.parseDouble(last);

        if (action.equals("+")) {
            result = (f + l);
        }
        if (action.equals("-")) {
            result = (f - l);
        }
        if (action.equals("*")) {
            result = (f * l);
        }
        if (action.equals("/")) {
            result = (f / l);
        }
        if (action.equals("/")) {
            if (l != 0) {
                result = (f / l);
            } else {
                text.setText("*Oшибка*");
                return;
            }
        }
        text.setText("" + result);
    }
}

