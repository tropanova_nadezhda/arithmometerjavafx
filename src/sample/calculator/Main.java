package sample.calculator;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Класс для реализации запуска приложения калькулятор
 *
 * @author Tropanova N.S.
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("program.fxml"));
        primaryStage.getIcons().add(new Image("sample\\assets\\calc.png"));
        primaryStage.setTitle("Калькулятор");
        primaryStage.setScene(new Scene(root, 250, 400));
        primaryStage.setResizable(false);
        primaryStage.show();

    }
}