package sample.arithmometer;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import sample.animation.Shake;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField firstOperand;

    @FXML
    private TextField secondOperand;

    @FXML
    private TextField result;

    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonClear;

    @FXML
    void add(ActionEvent event) {
        if (firstOperand.getText().isEmpty() || secondOperand.getText().isEmpty()) {
            Shake firstOperandAnim = new Shake(firstOperand);
            Shake secondOperandAnim = new Shake(secondOperand);
            firstOperandAnim.playAnimation();
            secondOperandAnim.playAnimation();
            return;
        }
        int first = Integer.parseInt(firstOperand.getText());
        int second = Integer.parseInt(secondOperand.getText());
        int sum = first + second;
        result.setText(String.valueOf(sum));
    }

    @FXML
    void clear(ActionEvent event) {
        firstOperand.clear();
        secondOperand.clear();
        result.clear();


    }


}


