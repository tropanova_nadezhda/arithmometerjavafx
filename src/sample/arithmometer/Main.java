package sample.arithmometer;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;


/**
 * Класс для реализации запуска приложения
 *
 * @author Tropanova N.S.
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("arithmometer/app.fxml"));
        primaryStage.getIcons().add(new Image("sample\\assets\\calc.png"));
        primaryStage.setTitle("Арифмометр");
        primaryStage.setScene(new Scene(root, 300, 500));
        primaryStage.setResizable(false);
        primaryStage.show();

    }
}